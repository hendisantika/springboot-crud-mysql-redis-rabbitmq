package com.hendisantika.event;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-crud-mysql-redis-rabbitmq
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 09/05/22
 * Time: 05.06
 */
public class UserDeletionEvent<User> extends EntityEvent<User> {

    private final User user;

    public UserDeletionEvent(User user) {
        super(user);
        this.user = user;
    }

    public User getUser() {
        return user;
    }
}
