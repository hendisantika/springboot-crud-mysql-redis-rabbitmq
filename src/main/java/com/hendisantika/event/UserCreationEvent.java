package com.hendisantika.event;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-crud-mysql-redis-rabbitmq
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 09/05/22
 * Time: 05.05
 */
public class UserCreationEvent<User> extends EntityEvent<User> {

    private final User user;

    public UserCreationEvent(User user) {
        super(user);
        this.user = user;
    }

    public User getUser() {
        return user;
    }
}
