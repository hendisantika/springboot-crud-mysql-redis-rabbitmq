package com.hendisantika;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootCrudMysqlRedisRabbitmqApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootCrudMysqlRedisRabbitmqApplication.class, args);
    }

}
